var express = require('express');
var app = express();
var bodyParser = require("body-parser");
var requestJson = require("request-json");
var movimientosJSON = require ('./movimientosv2.json');
var usuariosJSON = require ('./usuarios.json');
var urlmlab = 'https://api.mlab.com/api/1/databases/techu/collections'
var collection = '/usuarios'
var apiKey = 'V2jjQxqFfeDGl4OTpPn8oLLQvYvZfatS'


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

app.use(bodyParser.json());

app.get('/v3/usuarios', function(req, res) {

  clientmlab = requestJson.createClient(urlmlab + "/usuarios?apiKey=" + apiKey)

  clientmlab.get('', function(err, resM, body) {
    res.send(body)
  })

})

app.get('/v3/usuarios/:id', function(req, res) {

  clientmlab = requestJson.createClient(urlmlab + "/usuarios")

  clientmlab.get('?q={"idusuario":' + req.params.id + '}&apiKey=' + apiKey,
  function(err, resM, body) {
    res.send(body)
  })

})

app.post('/v3/usuarios', function(req, res) {

  clientmlab = requestJson.createClient(urlmlab + "/usuarios?apiKey=" + apiKey)

  clientmlab.post('', req.body, function(err, resM, body) {
    res.send(body)
  })

})

app.put('/v3/usuarios/:id', function(req, res) {

  clientmlab = requestJson.createClient(urlmlab + "/usuarios")
  var cambio = '{"$set":' + JSON.stringify(req.body) + '}'

  clientmlab.put('?q={"idusuario": ' + req.params.id + '}&apiKey=' + apiKey, JSON.parse(cambio), function(err, resM, body) {
    res.send(body)
  })

})

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

app.listen(4000);

console.log("Escuchando en el puerto 4000");

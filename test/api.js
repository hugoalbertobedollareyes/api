var mocha = require('mocha')
var chai = require('chai')
var chaiHttp = require('chai-http')
var server = require('../server')

var should = chai.should()


chai.use(chaiHttp)

describe('Connection testing', () => {

  it('Conecta con google', (done) => {

    chai.request('http://www.google.com.mx').get('/').end((err, res) => {

    	res.should.have.status(200)
    	done()

    })

  })

})

describe('Tests', () => {

  it('Root contesta', (done) => {

    chai.request('http://localhost:3000').get('/v3').end((err, res) => {
          
	res.should.have.status(200)
        done()

    })

  })

  it('Root funciona', (done) => {

    chai.request('http://localhost:3000').get('/v3').end((err, res) => {
          
	res.should.have.status(200)
        res.body.should.be.a('array')
        done()
	
     })

  })

  it('Root devuelve 2 colecciones', (done) => {

    chai.request('http://localhost:3000').get('/v3').end((err, res) => {

          res.should.have.status(200)
          res.body.should.be.a('array')
          res.body.length.should.be.eql(2)
          done()

        })

  })

  it('Root retorna los objetos correctos', (done) => {

    chai.request('http://localhost:3000').get('/v3').end((err, res) => {

          res.should.have.status(200)
          res.body.should.be.a('array')
          res.body.length.should.be.eql(2)

          for (var i = 0; i < res.body.length; i++) {

            res.body[i].should.have.property('recurso')
            res.body[i].should.have.property('url')

          }

          done()

        })
  })

})

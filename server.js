var movimientosJSON = require('./movimientosv2.json')
var usuariosJSON = require('./usuarios.json')
var express = require('express')
var bodyparser = require('body-parser')
var jsonQuery = require('json-query')
var requestJson = require('request-json')

var app = express()
app.use(bodyparser.json())

var urlmlab = "https://api.mlab.com/api/1/databases/techu/collections"
var apiKey = "V2jjQxqFfeDGl4OTpPn8oLLQvYvZfatS"
var clientmlab = requestJson.createClient(urlmlab + "?apiKey=" + apiKey)


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

app.get('/', function(req, res) {

  res.send('Hello API')

})

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

app.get('/v1/movimientos', function(req, res) {

  res.sendfile('movimientosv1.json')

})


app.get('/v2/movimientos', function(req, res) {

  res.send(movimientosJSON)

})


app.get('/v2/movimientos/:id', function(req, res) {

  console.log(req.params.id)
  res.send(movimientosJSON[req.params.id-1])

})


app.get('/v2/movimientosq', function(req, res) {

  console.log(req.query)
  res.send("received")

})


app.get('/v2/movimientosp/:id/:nombre', function(req, res) {

  console.log(req.params)
  res.send("received")

})


app.post('/v2/movimientos', function(req, res) {

  var nuevo = req.body
  nuevo.id = movimientosJSON.length + 1
  movimientosJSON.push(nuevo)
  res.send("Movimiento dado de alta")

})

app.put('/v2/movimientos/:id', function(req, res) {

  var cambios = req.body
  var actual = movimientosJSON[req.params.id-1]

  if (cambios.importe != undefined) {
    actual.importe = cambios.importe
  }

  if (cambios.ciudad != undefined) {
    actual.ciudad = cambios.ciudad
  }

  res.send("Cambios realizados")

})

app.delete('/v2/movimientos/:id', function(req, res) {

  var actual = movimientosJSON[req.params.id-1]

  movimientosJSON.push({
    "id": movimientosJSON.length+1,
    "ciudad": actual.ciudad,
    "importe": actual.importe * (-1),
    "concepto": "Negativo del " + req.params.id
  })

  res.send("Movimiento eliminado")

})


app.post('/v2/usuarios/login', function(req, res) {

  var email = req.headers['email']
  var password = req.headers['password']
  var resultados = jsonQuery('[email=' + email + ']', {data:usuariosJSON})

  if (resultados.value != null && resultados.value.password == password) {

    usuariosJSON[resultados.value.id-1].estado='logged'
    res.send('{"login:"OK"}')

  } else {

    res.send('{"login":"error"}')

  }

})

app.post('/v2/usuarios/logout/:id', function(req, res) {

  var id = req.params.id
  var usuario = usuariosJSON[id-1]

  if (usuario.estado=='logged') {

    usuario.estado = 'logout'
    res.send('{"logout":"OK"}')

  } else {
    	res.send('{"logout":"error"}')
  }

})


app.get('/v2/usuarios/:id', function(req, res) {

  console.log(req.params.id)
  res.send(usuariosJSON[req.params.id-1])

})

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

app.get('/v3', function(req, res) {

  clientmlab.get('', function(err, resM, body) {

    var coleccionesUsuario = []

    if (!err) {

      for (var i = 0; i < body.length; i++) {

        if (body[i] != "system.indexes") {
          coleccionesUsuario.push({"recurso":body[i], "url":"/v3/" + body[i]})
        }

      }

      res.send(coleccionesUsuario)

    } else {

      res.send(err)

    }

  })

})


app.get('/v3/usuarios', function(req, res) {

  clientmlab = requestJson.createClient(urlmlab + "/usuarios?apiKey=" + apiKey)

  clientmlab.get('', function(err, resM, body) {
    res.send(body)
  })

})

app.get('/v3/usuarios/:id', function(req, res) {

  clientmlab = requestJson.createClient(urlmlab + "/usuarios")

  clientmlab.get('?q={"idusuario":' + req.params.id + '}&apiKey=' + apiKey,
  function(err, resM, body) {
    res.send(body)
  })

})

app.post('/v3/usuarios', function(req, res) {

  clientmlab = requestJson.createClient(urlmlab + "/usuarios?apiKey=" + apiKey)

  clientmlab.post('', req.body, function(err, resM, body) {
    res.send(body)
  })

})

app.put('/v3/usuarios/:id', function(req, res) {

  clientmlab = requestJson.createClient(urlmlab + "/usuarios")
  var cambio = '{"$set":' + JSON.stringify(req.body) + '}'

  clientmlab.put('?q={"idusuario": ' + req.params.id + '}&apiKey=' + apiKey, JSON.parse(cambio), function(err, resM, body) {
    res.send(body)
  })

})

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

app.get('/v4', function(req, res) {
  res.sendfile('archivo.pdf')
})

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

app.listen(3000)
console.log("Escuchando en el puerto 3000")
